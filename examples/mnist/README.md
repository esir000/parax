MNIST Classification
====================

Modified from [https://github.com/google/flax/tree/master/examples/mnist](https://github.com/google/flax/tree/master/examples/mnist)

```bash
python main.py --workdir=/tmp/mnist --config=config.py
```
