## Instructions

1. Start ray cluster
```
# On head node
ray start --head
# (Optional) : launch worker nodes
```

2. Run benchmark
```
python3 benchmark_transformer_layer.py
```

